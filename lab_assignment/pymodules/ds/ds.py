'''python module providing basic data structure implementation stack, queue'''


class Stack:
    def __init__(self):
        self.lst = []

    def push(self, *elem):
        for each_item in elem:
            if type(each_item) == list:
                self.lst.extend(each_item)
            else:
                self.lst.append(each_item)

    def pop(self):
        return self.lst.pop()

    def is_empty(self):
        return self.lst == []

    def peek(self):
        return self.lst[len(self.lst) - 1]

    def size(self):
        return len(self.lst)

    def __add__(self, other):
        newlst = self.lst + other.lst
        newst = Stack()
        newst.push(newlst)
        return newst

    def __radd__(self, other):
        newlst = self.lst
        newst = Stack()
        newst.push(newlst)
        return newst

    def __str__(self):
        return str(self.lst)


class Queue:
    def __init__(self):
        self.lst = []

    def is_empty(self):
        return self.lst == []

    def enqueue(self, *elem):
        for each_item in elem:
            if type(each_item) == list:
                self.lst.extend(each_item)
            else:
                self.lst.append(each_item)

    def dequeue(self):
        return self.lst.pop(0)

    def size(self):
        return len(self.lst)

    def __str__(self):
        return str(self.lst)

    def __add__(self, other):
        newlist = self.lst + other.lst
        newq = Queue()
        newq.enqueue(newlist)
        return newq

    def __radd__(self, other):
        newq = Queue()
        newq.enqueue(self.lst)
        return newq

