#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/sem.h>
#include <sys/shm.h>

using namespace std;

key_t semkey = ftok(".", 'S');
key_t shmkey = ftok(".", 'M');

int semid = semget(semkey, 3, 0666|IPC_CREAT);
int shmid = shmget(shmkey, sizeof(int), 0666|IPC_CREAT);
int *sharedMemory;

long long int fact(int n)
{
	long long int f;
	if(n==0)
	{
		f = 0;
	}
	else
	{
		f = 1;
		for(int i=1;i<=n;i++)
		{
			f *= i;
		}
	}
	return f;
}

union semun
{
	int value;  //for GETVAL SETVAL
	struct semid_ds *buf; //for IPC_STAT IPC_SET, not needed
	ushort *array; //for GETALL SETALL
};

void initsem()
{
	if(semid < 0)
	{
		perror("cannot create semaphore");
		_exit(1);
	}
	union semun sem;
	sem.array = new ushort[4];
	sem.array[0] = 0;	//ensures concurrency	
	sem.array[1] = 1;	//ensures mutual exclusion
	sem.array[2] = 0;	//ensures factorials to be printed only after increment, also used to run n+1 th process as pseudo-daemon
	if(semctl(semid, 0, SETALL, sem) < 0)
	{
		perror("cannot set semaphore values");
		_exit(2);
	}
}

void initshm(int n)
{
	if(shmid < 0)
	{
		perror("cannot create shared memory");
		_exit(6);
	}
	sharedMemory = (int*)shmat(shmid, NULL, 0);
	sharedMemory[0] = 0;	//used for factorial operation
	shmdt(sharedMemory);
}

void Wait(int semid, int semnum)
{
	struct sembuf operation = {semnum, -1, 0};
	if(semop(semid, &operation, 1) < 0)
	{
		perror("semop cannot be applied");
		_exit(3);
	}
}

void Signal(int semid, int semnum, int n)
{
	struct sembuf operation = {semnum, n, 0};
	if(semop(semid, &operation, 1) < 0)
	{
		perror("semop cannot be applied");
		_exit(4);
	}
}

void removesem()
{
	if(semctl(semid, 0, IPC_RMID) < 0)
	{
		perror("cannot remove semaphore");
		_exit(5);
	}
}

void removeshm()
{
	shmctl(shmid,IPC_RMID, NULL);
}

int main(int argc, char* argv[])
{
	int n=atoi(argv[1]);
	initsem();
	initshm(n);
	for(int i=1;i<=n+1;i++)
	{
		pid_t pid = fork();
		if(pid == 0)
		{
			if(i==n+1)
			{
				Signal(semid, 0, n); //n+1 th process sends signal that all process has been created
				int* sh = (int*)shmat(shmid, NULL, 0);
				while(true)
				{
					Wait(semid, 2); //waits for a child process to compute factorial
					cout << fact(sh[0]) << endl;
					if(semctl(semid, 2, GETVAL, 0) == 1) //if last value has been calculated then break
						break;
					Signal(semid, 1, 1); //n+1 th process says it has done printing
				}
				shmdt(sh);
			}
			else
			{
				Wait(semid, 0); //waits until n+1 th process gives signal
				Wait(semid, 1);	//waits until n+1 th process says it has done printing, initially 1
				int* sh = (int*)shmat(shmid, NULL, 0);
				sh[0] ++;
				if(sh[0] == n)
					Signal(semid, 2, 2); //if the last value then increment semaphore by 2
				else
					Signal(semid, 2, 1); //else increment by 1
				shmdt(sh);
			}
			_exit(0);
		}
	}
	while (waitpid(-1, 0, 0) >= 0){} //reaps child at one go
	removesem();
	removeshm();
	return 0;
}
