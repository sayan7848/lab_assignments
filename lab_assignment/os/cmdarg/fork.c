#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char* argv[], char* envp[])
{
		int pid = getpid(), i;
		for (i=1;i<argc;i++)
		{
				int status = fork();
				if (status == 0)
				{
						execl("/bin/sh","sh","-c",argv[i],(char*)0);
						exit(0);
				}
				else
				{
						waitpid(status,0,0);
				}
		}
}
