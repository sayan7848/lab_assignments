#include <string>
#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <vector>
#include <sstream>
#include <cstring>
#include <sys/wait.h>

using namespace std;


int main(int argc, char* argv[])
{
	int status, pfd[2];
	pipe(pfd);
	if (fork() == 0)
	{
			close(pfd[1]);
			dup2(pfd[0], 0);
			close(pfd[0]);
			execlp("wc", "wc" ,(char*) 0);
			exit(0);
	}
	else
	{
			close(pfd[0]);
			dup2(pfd[1], 1);
			close(pfd[1]);
			execlp("ls", "ls", (char*) 0);
			wait(&status);
	}
	return 0;
}
