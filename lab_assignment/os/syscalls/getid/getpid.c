#include <sys/types.h> 
#include <unistd.h>
#include <stdio.h>

void childProcess()
{
	printf("parent %d   child %d\n",getppid(),getpid());
}

int main()
{
	printf("parent is forking a process\n");
	fork();
	childProcess();
	return 0;
}

