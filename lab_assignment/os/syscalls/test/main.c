#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[], char* envp[])
{
	int i,status;
	for (i = 0; argv[i] != (char*)0; i++)
	{
		int child_id = fork();
		if (child_id == 0)
		{
			execv("/bin/", argv[i]);
		}
		else
		{
			wait(&status);
		}
	}
	return 0;
}

