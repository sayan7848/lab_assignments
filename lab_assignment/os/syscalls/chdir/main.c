#include <unistd.h> 
#include <stdio.h>
#include <stdlib.h>

int main()
{
	chdir("../");
	printf("directory changed to ../\n");
	FILE *demo_file = fopen("demo.txt","w");
	fprintf(demo_file,"this is a demo text\n");
	fclose(demo_file);
	return 0;
}
