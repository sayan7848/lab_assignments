#include <stdio.h>
#include <signal.h>
#include <unistd.h>

int reference = 10;
void onbell()
{
    reference++;
}

int main()
{
	printf("alarm for 5 seconds\n");
	signal(SIGALRM,onbell);
	unsigned int remainder = alarm(5);
	pause();
	printf("%d\n",reference);
}
