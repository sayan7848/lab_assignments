#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char* argv[], char* envp[])
{
	int pid = getpid(),status;
	char *homePath, *getenv();
	homePath = getenv("HOME");
	printf("%s\n",homePath);
	for (int i=1; argv[i] != (char*)0; i++)
	{
		int id = fork();
		wait(pid);
		if (id == 0)
		{
			//another way is to use system(command), it will create an extra fork though
		
			execl("/bin/sh", "sh", "-c", argv[i], (char*)0);
			_exit(0);
		}
		if(id > 0)
		{
			//parent forking child
		}
	}
}
