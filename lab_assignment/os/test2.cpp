#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <unistd.h>
#include <string>
#include <vector>
#include <sstream>
#include <cstring>
#include <sys/wait.h>

using namespace std;

void split(const string &s, char delim, vector<string> &elems) 
{
		stringstream ss(s);
    	string item;
    	while (getline(ss, item, delim)) 
		{
        	elems.push_back(item);
    	}
}

vector<string> split(const string &s, char delim) 
{
    	vector<string> elems;
    	split(s, delim, elems);
    	return elems;
}

int main()
{
		system("clear");
		while(true)
		{
			int pid;
			char *cwd = get_current_dir_name();
			string s;
			cout << cwd << " :-) ";
			getline(cin, s);
			if(s == "exit")
			{
					cout << "bye :-( " << endl;
					_exit(0);
			}
			int i;
			vector<string> v = split(s, ' ');
			char** argv = new char*[v.size() + 1];
			for(i=0; i<v.size(); i++)
			{
					argv[i] = new char[v[i].length() + 1];
					strcpy(argv[i], v[i].c_str());
			}
			argv[i] = new char;
			argv[i] = NULL;

			if(strcmp("cd",argv[0]) == 0)
			{
					if(argv[1] == NULL || strcmp(argv[1],"~") == 0)
					{
							chdir(getenv("HOME"));
					}
					else
					{
						if(!chdir(argv[1]) == 0)
						{

								cout << "ERROR" << endl;
						}
					}
			}
			else
			{
				if((pid=fork()) == 0)
				{
						execvp(argv[0], argv);
						cout<< "ERROR" << endl;
						_exit(0);
				}
				else
				{
						waitpid(pid, 0, 0);
				}
			}
		}
		return 0;
}
