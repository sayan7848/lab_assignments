#include <cstring>
#include <sstream>
#include <fstream>
#include <vector>
#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <unistd.h>
#include <cstring>
#include <sys/wait.h>
#include <signal.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

using namespace std;

pid_t parentPid;

void split(string& s, vector<string>& v, char& delim)
{
	stringstream ss(s);
	string item;
	while(getline(ss,item,delim))
	{
		if(item != "")
			v.push_back(item);
	}
}

vector<string> split(string& s, char& delim)
{
	vector<string> v;
	split(s, v, delim);
	return v;
}

void modifyVector(vector<string>& v, bool& isBackground, bool& isInputRedirected, bool& isOutputRedirected, bool& isPiped, string& inputFileName, string& outputFileName, vector<int>& pipeIndex)
{
	if(isBackground)
	{
		v.pop_back();
	}
	if(isOutputRedirected)
	{
		outputFileName = v[v.size() - 1];
		v.pop_back();
		v.pop_back();
	}
	if(isInputRedirected)
	{
		inputFileName = v[v.size() - 1];
		v.pop_back();
		v.pop_back();
	}
}


bool handleBuiltIn(vector<string> &v)
{
	bool isBuiltIn = false;
	if(v[0] == "exit")
	{
		cout << "Bye :-( " << endl;
		isBuiltIn = true;
		_exit(1);
	}
	else if(v[0] == "cd")
	{
		isBuiltIn = true;
		if(v.size() == 1 || v[1] == "~")
		{
			chdir(getenv("HOME"));
		}
		else
		{
			chdir(v[1].c_str());
		}
	}
	return isBuiltIn;
}

void signalHandler(int sig) 
{
	if(sig == SIGCHLD)
	{
 		int saved_errno = errno;
  		while (waitpid((pid_t)(-1), 0, WNOHANG) > 0) {}
  		errno = saved_errno;
	}
	else if(sig == SIGINT)
	{
		kill(sig, parentPid);
	}
}

void analyzeSpecialOp(string& s, bool& isPiped, bool& isBackground, bool& isInputRedirected, bool& isOutputRedirected)
{
	isPiped = false;
	isBackground = false;
	isInputRedirected = false;
	isOutputRedirected = false;
	
	if(s.find("|") != -1)
		isPiped = true;
	if(s.find("<") != -1)
		isInputRedirected = true;
	if(s.find(">") != -1)
		isOutputRedirected = true;
	if(s.find("&") != -1)
		isBackground = true;
}

int main()
{
	char **initialArg = new char*[2];
	initialArg[0] = new char[strlen("clear")];
	strcpy(initialArg[0], "clear");
	initialArg[1] = NULL;
	int initialPid;
	if((initialPid = fork()) == 0)
		execvp(initialArg[0], initialArg);
	else
	{
		waitpid(initialPid, 0, 0);
		delete []initialArg;
	}
	cout << "usage : command <options> <options> ..." << endl;
	cout << "the usage of ' ' as delimitor is must" << endl;
	bool isInputRedirected = false, isOutputRedirected = false;
	int stdin_proxy = dup(STDIN_FILENO);
	int stdout_proxy = dup(STDOUT_FILENO);
	int inputFd, outputFd;
	char **argv = NULL;
	while(true)
	{
		if(cin.eof())
		{
			cout << endl;
			_exit(1);
		}
		int status;
		signal(SIGINT, signalHandler);
		cout << get_current_dir_name() << " :-) ";
		string s, inputFileName, outputFileName;
		bool isBackground, isPiped;
		getline(cin, s);
		if(s != "")
		{
			analyzeSpecialOp(s, isPiped, isBackground, isInputRedirected, isOutputRedirected);
			char delim = ' ';
			vector<string> v = split(s, delim);
			if(!handleBuiltIn(v))
			{
				vector<int> pipeIndex;
				modifyVector(v, isBackground, isInputRedirected, isOutputRedirected, isPiped, inputFileName, outputFileName, pipeIndex);
				argv = new char*[v.size() + 1];
				if(isBackground)
				{
					signal(SIGCHLD, signalHandler);
				}
				pid_t pid;
				if((parentPid = fork()) == 0)
				{
					if(isInputRedirected)
					{
						if((freopen(inputFileName.c_str(), "r", stdin)) == NULL)
						{
							perror("cant open file");
						}

					}
					if(isOutputRedirected)
					{
						if((freopen(outputFileName.c_str(), "w", stdout)) == NULL)
						{
							perror("cant open file");
						}
					}
					if(isPiped)
					{
						signal(SIGCHLD, signalHandler);
						string s = "";
						for(int i=0; i<v.size(); i++)
						{
							s += v[i] + " ";
						}
						char delim = '|';
						vector<string> temp_vec1 = split(s, delim);
						for(int i=0; i<temp_vec1.size(); i++)
						{
							char delim = ' ';
							vector<string> temp_vec2 = split(temp_vec1[i], delim);
							char **temp_argv = new char*[temp_vec2.size() + 1];
							for(int j=0; j<temp_vec2.size(); j++)
							{
								temp_argv[j] = new char[temp_vec2[j].length() + 1];
								strcpy(temp_argv[j], temp_vec2[j].c_str());
							}
							temp_argv[temp_vec2.size()] = NULL;
							int pfd[2];
							int fd;
							pipe(pfd);
							pid_t pid;
							if((pid = fork()) > 0)
							{
								fd = pfd[0];
								waitpid(pid, 0, 0);
								close(pfd[1]);
								delete []temp_argv;
								if(i == temp_vec1.size() - 1)
									_exit(1);
							}
							else
							{
								dup2(fd, 0);
								if (i != temp_vec1.size() - 1)
								{
									dup2(pfd[1], 1);
								}
								close(pfd[0]);
								close(pfd[1]);
								execvp(temp_argv[0], temp_argv);
							}
						}
					}
					if(!isPiped)
					{
						int i;
						for(i=0; i<v.size(); i++)
						{
							argv[i] = new char[v[i].length() + 1];
							strcpy(argv[i], v[i].c_str());
						}
						argv[i] = NULL;
						execvp(argv[0], argv);
						perror("ERROR IN BG PROCESS");
						_exit(1);
					}
				}
				else
				{
					if(!isBackground)
						waitpid(pid,0,0);
					delete []argv;
				}
			}
		}
	}
	return 0;
}
