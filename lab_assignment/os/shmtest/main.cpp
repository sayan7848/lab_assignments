#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <unistd.h>
#include <cstring>
#include <sstream>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/wait.h>

using namespace std;

key_t myKey = ftok(".", 'R');

void inputMatrix(int* sharedPtr, int& index, int& row, int& col)
{
	cout << "enter " << row << " * " << col << " matrix" << endl;
	for (int i=0; i<row; i++)
	{
		for (int j=0; j<col; j++)
		{
			cin >> sharedPtr[index++];
		}
	}
}

void displayMatrix(int* inputMatrix, int& readIndex, int& row, int& col)
{
	cout << endl;
	for (int i=0; i<row; i++)
	{
		for (int j=0; j<col; j++)
		{
			cout << inputMatrix[readIndex++] << " ";
		}
		cout << endl;
	}
}

int str2int(string s)
{
	istringstream mystream(s);
	int result = 0;
	mystream >> result;
	return result;
}

string int2str(int num)
{
	stringstream mystream;
	mystream << num;
	return mystream.str();
}


int main()
{
	int rowa,rowb,cola,colb,writeIndex = 3, readIndex = 3;
	int* sharedPtr;
	cout << "enter number of rows of matrix A" << endl;
	cin >> rowa;
	cout << "enter number of columns of matrix A" << endl;
	cin >> cola;
	cout << "enter number of rows of matrix B" << endl;
	cin >> rowb;
	cout << "enter number of columns of matrix B" << endl;
	cin >> colb;
	if (cola != rowb)
	{
		cout << "matrix multiplication not possible" << endl;
		_exit(1);
	}
	int size = (rowa * cola + rowb * colb + rowa * colb + 4) * sizeof(int);
	int shmId = shmget(myKey, size, IPC_CREAT | 0666);
	if (shmId == -1)
	{
		perror("error in shmget");
		_exit(2);
	}
	sharedPtr = (int*)shmat(shmId, NULL, 0);
	if ((char*)sharedPtr == (char*)-1)
	{
		perror("error in shmat");
		shmctl(shmId, IPC_RMID, NULL);
		_exit(3);
	}
	sharedPtr[0] = rowa;
	sharedPtr[1] = cola;
	sharedPtr[2] = colb;
	inputMatrix(sharedPtr, writeIndex, rowa, cola);
	inputMatrix(sharedPtr, writeIndex, rowb, colb);
	for (int i=0; i<rowa; i++)
	{
		for (int j=0; j<colb; j++)
		{
			pid_t pid;
			if ((pid = fork()) == 0)
			{
				char** argv = new char*[5];
				argv[0] = new char[strlen("./compute") + 1];
				strcpy(argv[0], "./compute");
				argv[1] = new char[int2str(i).length() + 1];
				strcpy(argv[1], int2str(i).c_str());
				argv[2] = new char[int2str(j).length() + 1];
				strcpy(argv[2], int2str(j).c_str());
				argv[3] = new char[int2str(size).length() + 1];
				strcpy(argv[3], int2str(size).c_str());
				argv[4] = NULL;
				execvp(argv[0], argv);	
			}
		}
	}
	while(waitpid(-1,0,WNOHANG)>=0){}
	readIndex = rowa * cola + rowb * colb + 3;
	displayMatrix(sharedPtr, readIndex, rowa, colb);
	shmdt(sharedPtr);
	shmctl(shmId, IPC_RMID, NULL);
	return 0;
}
