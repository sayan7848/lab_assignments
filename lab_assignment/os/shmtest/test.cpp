#include<iostream>
#include<cstdlib>
#include<cstdio>
#include<cstring>
#include<unistd.h>
#include<sys/wait.h>

int main()
{
	char** argv = new char*[3];
	argv[0] = new char[strlen("write") + 1];
	argv[1] = new char[20];
	strcpy(argv[0],"write");
	strcpy(argv[1],"alok57");
	argv[2] = NULL;
	pid_t pid;
	//execvp(argv[0],argv);
	if((pid = fork()) == 0)
	{
		execvp(argv[0],argv);	
	}
	else
	{
		wait(&pid);	
		char **argv1 = new char*[2];
		argv1[0] = new char[strlen("ls") + 1];
		argv1[1] = NULL;
		execvp(argv1[0],argv1);
	}

	return 0;
}
