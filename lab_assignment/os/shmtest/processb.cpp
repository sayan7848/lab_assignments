#include <sys/ipc.h>
#include <sys/shm.h>
#include <signal.h>
#include <unistd.h>
#include <iostream>
#include <cstdio>
#include <cstdlib>

using namespace std;

int main()
{
	key_t mykey = ftok(".",'R');
	int shmid = shmget(mykey, sizeof(pid_t), IPC_CREAT | 0666);
	pid_t* shmptr = (pid_t*)shmat(shmid, NULL, 0);
	pid_t pid = *shmptr;
	shmdt(shmptr);
	while(true)
	{
		string s;
		cout << "kill or interrupt??" << endl;
		getline(cin, s);
		if(s[0] == 'i' || s[0] == 'I')
		{
			cout << "sent interrupt to other process" << endl;
			kill(pid, SIGINT);
		}
		else if(s[0] == 'k' || s[0] == 'K')
		{
			cout << "sent quit to other process" << endl;
			kill(pid, SIGQUIT);
			_exit(0);
		}
		else
		{
			cout << "wrong choice" << endl;
		}
	}
	return 0;
}
