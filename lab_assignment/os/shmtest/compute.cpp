#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <sstream>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>

using namespace std;

key_t myKey = ftok(".",'R');

void decodeArg(char* argv[], int& row, int& col, int& size)
{
	istringstream stream1(argv[1]);
	istringstream stream2(argv[2]);
	istringstream stream3(argv[3]);
	stream1 >> row;
	stream2 >> col;
	stream3 >> size;
}

int main(int argc, char* argv[])
{
	int row, col, size;
	decodeArg(argv, row, col, size);
	int shmId = shmget(myKey, size, IPC_CREAT | 0666);
	if (shmId == -1)
	{
		perror("error in shmget");
		_exit(1);
	}
	int* sharedPtr = (int*)shmat(shmId, NULL, 0);
	if ((char*)sharedPtr == (char*)-1)
	{
		perror("error in shmat");
		_exit(2);
	}
	int sum = 0;
	int matrix_row = sharedPtr[0];
	int common = sharedPtr[1];
	int matrix_column = sharedPtr[2];
	int offset = matrix_row * common;
	int result_offset = matrix_row * common + common * matrix_column + 3;
	for (int i=0; i<common; i++)
	{
		sum += sharedPtr[common * row + i + 3] * sharedPtr[offset + matrix_column * i + col + 3];
	}
	sharedPtr[result_offset + row * matrix_column + col] = sum;
	shmdt(sharedPtr);
	return 0;
}
