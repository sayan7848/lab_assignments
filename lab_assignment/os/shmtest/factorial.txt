result of n = 3

14274 returns product  = 1
14274 returns product  = 1
14275 has incremented 1
14274 returns product  = 1
******************
14275 returns product  = 1
14278 has incremented 1
14275 returns product  = 1
******************
14278 returns product  = 1
******************
14279 has incremented 2
******************
14276 has incremented 3
14277 has incremented 4
******************
14276 returns product  = 24
******************
14281 has incremented 5
******************
14280 has incremented 6
******************

Target - Print the factorials of all numbers from 0 to n maintaining order.

Observation - The calculation and order of printing is unpredictable.

Reasoning - The processes share a variable through shared memory. No locking or synchronization mechanism has been implemented. Thus if context switch occurs after incrementation of the shared variable by a child and before calculation of factorial, erroneous rsult may appear.

Solution - Synchronization mechanism like semaphore should be implemented.
