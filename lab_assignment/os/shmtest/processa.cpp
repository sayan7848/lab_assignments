#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <signal.h>
#include <unistd.h>

using namespace std;

int shmid;
pid_t* shmptr;

void signalHandler(int signum)
{
	if(signum == SIGINT)
	{
		signal(signum, SIG_IGN);
		cout << "recieved INTERRUPT signal" << endl;
		signal(signum, signalHandler);
	}
	if(signum == SIGQUIT)
	{
		signal(signum, SIG_IGN);
		cout << "got a KILL signal" << endl;
		shmdt(shmptr);
		shmctl(shmid, IPC_RMID, NULL);
		_exit(1);
	}
}

int main()
{
	pid_t pid = getpid();
	key_t myKey = ftok(".", 'R');
	if(signal(SIGINT, signalHandler) == SIG_ERR)
	{
		perror("SIGINT");
		_exit(1);
	}
	if(signal(SIGQUIT, signalHandler) == SIG_ERR)
	{
		perror("SIGQUIT");
		_exit(1);
	}
	shmid = shmget(myKey, sizeof(pid_t), IPC_CREAT | 0666);
	shmptr = (pid_t*)shmat(shmid, NULL, 0);
	*shmptr = pid;
	int i = 0;
	while(true)
	{
		cout << "process a " << i << endl;
		sleep(1);
		i++;
	}
	return 0;
}
