#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <unistd.h>
#include <sstream>
#include <sys/ipc.h>
#include <sys/shm.h>

using namespace std;

int factorial(int num)
{
	int product = 1;
	for (int i=1; i<=num; i++)
	{
		product *= i;
	}
	return product;
}

int main(int argc, char* argv[])
{
	istringstream mystream(argv[1]);
	int n;
	mystream >> n;
	key_t myKey = ftok(".", 'R');
	int shmid = shmget(myKey, sizeof(int), IPC_CREAT | 0666);
	int* sharedInt = (int*)shmat(shmid, NULL, 0);
//	sharedInt[0] = 0;
	*sharedInt = 0;
	shmdt(sharedInt);
	for (int i=0; i<n; i++)
	{
		if(fork() == 0)
		{
			int shmid = shmget(myKey, sizeof(int), IPC_CREAT | 0666);
			int* sharedInt = (int*)shmat(shmid, NULL, 0);
		//	sharedInt[0] ++;
			(*sharedInt)++;
			cout << getpid() << " has incremented " << *sharedInt << endl;
			shmdt(sharedInt);
		}
		else
		{
			int shmid = shmget(myKey, sizeof(int), IPC_CREAT | 0666);
			int* sharedInt = (int*)shmat(shmid, NULL, 0);
			int product = factorial(*sharedInt);
			cout << getpid() << " returns product  = " << product << endl;
			shmdt(sharedInt);
		}
	}
	cout<<"******************"<<endl;
	shmctl(shmid, IPC_RMID, NULL);
	return 0;
}
