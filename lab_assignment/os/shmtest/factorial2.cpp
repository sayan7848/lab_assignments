#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <unistd.h>
#include <sstream>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <sys/sem.h>
#include <sys/wait.h>
#define SEMKEY 7848
#define SHMKEY 8487

using namespace std;

union semun
{
	int semval;
	struct semid_ds *buf;
	ushort *array;
};

void initsem()
{
	union semun sem;
	sem.semval = 0;
	int semid = semget(SEMKEY, 2, 0666|IPC_CREAT);
	if(semid<0)
	{
		perror("cannot create semaphore");
		_exit(1);
	}
	if(semctl(semid, 0, SETVAL, sem) < 0)
	{
		perror("cannot set semaphore 0");
		_exit(2);
	}
	sem.semval = 1;
	if(semctl(semid, 1, SETVAL, sem) < 0)
	{
		perror("cannot set semaphore 1");
		_exit(3);
	}
}

void initshm()
{
	int shmid = shmget(SHMKEY, sizeof(int), 0666|IPC_CREAT);
	if(shmid<0)
	{
		perror("cannot create shared memory");
		_exit(3);
	}
	int* sharedPtr = (int*)shmat(shmid, NULL, 0);
	if(*sharedPtr == -1)
	{
		perror("cannot attatch to shared memory");
		_exit(4);
	}
	sharedPtr[0] = 0;
	if(shmdt(sharedPtr) == -1)
	{
		perror("cannot detatch initialized shared memory");
		_exit(5);
	}
}


int factorial(int num)
{
	int product = 1;
	for (int i=1; i<=num; i++)
	{
		product *= i;
	}
	return product;
}

struct sembuf operation[1];

void Wait(int semid, int semnum)
{	
	operation[0].sem_num = semnum;
	operation[0].sem_op = -1;
	operation[0].sem_flg = 0;
	semop(semid, operation, 1);
}

void Signal(int semid, ushort semnum)
{
	operation[0].sem_num = semnum;
	operation[0].sem_op = 1;
	operation[0].sem_flg = 0;
	semop(semid, operation, 1);
}

void removeshm()
{
	int shmid = shmget(SHMKEY, sizeof(int), 0666|IPC_CREAT);
	shmctl(shmid, IPC_RMID, NULL);
}

void removesem()
{
	int semid = semget(SEMKEY, 2, 0666|IPC_CREAT);
	semctl(semid, 0, IPC_RMID, 0);
	semctl(semid, 1, IPC_RMID, 0);
}


int main(int argc, char* argv[])
{
	initsem();
	initshm();
	int shmid = shmget(SHMKEY, sizeof(int), IPC_CREAT | 0666);
	int* sharedInt = (int*)shmat(shmid, NULL, 0);
	int n = atoi(argv[1]);
	int semid = semget(SEMKEY, 2, 0666);
	for (int i=1; i<=n; i++)
	{
		if(fork() == 0)
		{ 
			if(semid < 0)
			{
				perror("semaphore not found, ran init?");
				_exit(1);
			}
			Wait(semid, 1); 						
			sharedInt[0] ++;
			//shmdt(sharedInt);
			Signal(semid, 0);
			_exit(0);

		}
		else
		{
			Wait(semid,0);
			if(semid < 0)
			{
				perror("semaphore not found, ran init?");
				_exit(2);
			}
			//	int shmid = shmget(SHMKEY, sizeof(int), IPC_CREAT | 0666);
			//	int* sharedInt = (int*)shmat(shmid, NULL, 0);
				int product = factorial(sharedInt[0]);
				cout << product << endl;
			//	shmdt(sharedInt);
				Signal(semid,1);
			
		}
	}
	while(waitpid(-1,0,0)>=0){}
	removesem();
	removeshm();
	return 0;
}
