#include<sys/ipc.h>
#include<sys/types.h>
#include<sys/sem.h>
#include<sys/shm.h>
#include<unistd.h>
#include<cstdio>
#define SEMKEY 7848
#define SHMKEY 8487

union semun
{
	int semval;
	struct semid_ds *buf;
	ushort *array;
};

void initsem()
{
	union semun sem;
	sem.semval = 0;
	int semid = semget(SEMKEY, 2, 0666|IPC_CREAT);
	if(semid<0)
	{
		perror("cannot create semaphore");
		_exit(1);
	}
	if(semctl(semid, 0, SETVAL, sem) < 0)
	{
		perror("cannot set semaphore 0");
		_exit(2);
	}
	sem.semval = 1;
	if(semctl(semid, 1, SETVAL, sem) < 0)
	{
		perror("cannot set semaphore 1");
		_exit(3);
	}
}

void initshm()
{
	int shmid = shmget(SHMKEY, sizeof(int), 0666|IPC_CREAT);
	if(shmid<0)
	{
		perror("cannot create shared memory");
		_exit(3);
	}
	int* sharedPtr = (int*)shmat(shmid, NULL, 0);
	if(*sharedPtr == -1)
	{
		perror("cannot attatch to shared memory");
		_exit(4);
	}
	sharedPtr[0] = 0;
	if(shmdt(sharedPtr) == -1)
	{
		perror("cannot detatch initialized shared memory");
		_exit(5);
	}
}

int main()
{
	initsem();
	initshm();
	return 0;
}
