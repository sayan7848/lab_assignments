#include<sys/shm.h>
#include<sys/sem.h>
#include<sys/types.h>
#include<sys/ipc.h>
#include<unistd.h>
#include<cstdio>
#define SHMKEY 8487
#define SEMKEY 7848

void removeshm()
{
	int shmid = shmget(SHMKEY, sizeof(int), 0666|IPC_CREAT);
	shmctl(shmid, IPC_RMID, NULL);
}

void removesem()
{
	int semid = semget(SEMKEY, 2, 0666|IPC_CREAT);
	semctl(semid, 0, IPC_RMID, 0);
	semctl(semid, 1, IPC_RMID, 0);
}

int main()
{
	removesem();
	removeshm();
	return 0;
}
