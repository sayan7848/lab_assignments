#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <unistd.h>
#include <sys/wait.h>

int main()
{
		if(fork() == 0)
				exit(0);
		else
				while(1);
		return 0;
}
