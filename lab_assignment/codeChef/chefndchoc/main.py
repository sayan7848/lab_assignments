test_cases = int(input())
result_list = []
for i in range(test_cases):
    lst = list(map(int, input().split()))
    if(lst[0] % 2 == 0 or lst[1] % 2 == 0):
        result_list.append("Yes")
    else:
        result_list.append("No")
for each_item in result_list:
    print(each_item)
