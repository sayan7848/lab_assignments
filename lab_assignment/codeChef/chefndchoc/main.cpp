#include<iostream>
#include<vector>
#include<cstdio>
#include<cstdlib>

using namespace std;

int main()
{
	int test_cases;
	cin >> test_cases;
	vector<string> result_vector;
	for(int i=0;i<test_cases;i++)
	{
		int row,col;
		cin >> row >> col;
		if(row%2==0 || col%2 ==0)
			result_vector.push_back("Yes");
		else
			result_vector.push_back("No");
	}
	for(int i=0;i<result_vector.size();i++)
		cout << result_vector[i] << endl;
	return 0;
}
