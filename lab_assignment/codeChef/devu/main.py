(n, q) = tuple(map(int, input().split()))
num_list = list(map(int, input().split()))
max_num = max(num_list)
min_num = min(num_list)
result_list = []
for i in range(q):
    test = int(input())
    if test >= min_num and test <= max_num:
        result_list.append("Yes")
    else:
        result_list.append("No")
for each_item in result_list:
    print(each_item)
