import itertools


def analyze(lst=[], row=0, col=0):
    sum = 0
    for j in range(col):
        temp_like_list = []
        for i in range(row):
            if lst[i][j] is 1:
                temp_like_list.append(i)
        if len(temp_like_list) > 1:
            comb_list = list(itertools.combinations(temp_like_list, 2))
            sum += len(comb_list)
    return sum

t = int(input())
result_list = []
for i in range(t):
    (n, m) = map(int, input().split())
    input_list = []
    for j in range(n):
        temp_list = []
        temp_string = input()
        for each_item in temp_string:
            temp_list.append(int(each_item))
        assert len(temp_list) == m
        input_list.append(temp_list)
    assert len(input_list) == n
    result_list.append(analyze(input_list, n, m))
for each_item in result_list:
    print(each_item)
