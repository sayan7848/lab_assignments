t = int(input())
for i in range(t):
    (n, k, e, m) = tuple(map(int, input().split()))
    if k >= n:
        print(0)
    else:
        marks_list = []
        for j in range(n - 1):
            temp = list(map(int, input().split()))
            marks_list.append(sum(temp))
        sergy_marks = sum(list(map(int, input().split())))
        marks_list.sort(reverse=True)
        marks_to_get = marks_list[k - 1] + 1 - sergy_marks
        if marks_to_get < 0:
            print(0)
        elif marks_to_get >= 0 and marks_to_get <= m:
            print(marks_to_get)
        else:
            print("Impossible")
