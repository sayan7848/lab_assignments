t = int(input())
result_list = []
for i in range(t):
    (n, m) = map(int, input().split())
    result_list.append(n * (m-1) + m * (n-1))
for each_item in result_list:
    print(each_item)
