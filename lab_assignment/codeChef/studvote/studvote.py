def myfunc(t):
    x = int(t)
    return x - 1


t = int(input())
result_list = []
for i in range(t):
    (n, k) = tuple(map(int, input().split()))
    a = list(map(myfunc, input().split()))
    assert len(a) == n
    candidate_list = [0]*n
    for i in range(len(a)):
        if candidate_list[a[i]] >= 0:
            if a[i] != i:
                candidate_list[a[i]] += 1
            else:
                candidate_list[a[i]] = -999
    count = 0
    for each_item in candidate_list:
        if each_item >= k:
            count += 1
    result_list.append(count)
for each_item in result_list:
    print(each_item)
