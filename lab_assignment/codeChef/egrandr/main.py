def analyze(lst=[]):
    if 2 in lst:
        return False
    if 5 in lst:
        avg = sum(lst) / len(lst)
        if avg >= 4.0:
            return True
        else:
            return False
    else:
        return False

result_list = []
t = int(input())
for i in range(t):
    n = int(input())
    score_list = list(map(int, input().split()))
    assert len(score_list) == n
    if analyze(score_list):
        result_list.append("Yes")
    else:
        result_list.append("No")
for each_item in result_list:
    print(each_item)
