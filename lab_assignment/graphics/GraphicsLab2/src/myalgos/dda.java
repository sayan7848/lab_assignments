package myalgos;

import myops.modify;

import java.awt.*;

/**
 * Created by sayan on 31/8/16.
 */
public class dda {

    public dda(Graphics g, Point p1, Point p2, Color color, int gap, int offset)
    {
        int x1 = p1.x, y1 = p1.y, x2 = p2.x, y2 = p2.y, x = x1, y = y1, key = 0;
        g.setColor(color);
        int dx = x2 - x1, dy = y2 - y1;
        int steps = dx > dy ? Math.abs(dx) : Math.abs(dy);
        float x_inc = dx/steps, y_inc = dy/steps;
        for(int i=0; i<steps; i++)
        {
            x += x_inc;
            y += y_inc;
            int x_mod = modify.modifyx((int)Math.ceil(x), gap);
            int y_mod = modify.modifyy((int)Math.ceil(y), gap, offset);
            try{
                if(Math.abs(y1-y2)>10){
                    if(y_mod != key)  {
                        g.fillRect(x_mod, y_mod, gap, gap);
                        key = y_mod;
                    }
                }
                else{
                    g.fillRect(x_mod, y_mod, gap, gap);
                }

            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }
    }
}
