package myalgos;

import myops.modify;

import java.awt.*;

/**
 * Created by sayan on 31/8/16.
 */
public class mpcircle {

    public mpcircle(Graphics g, Point center, int radius, Color color, int gap, int offset) {
        g.setColor(color);
        int x0 = center.x, y0 = center.y;
        int x = 0;
        int y = radius;
        float decision = 5 / 4 - radius;
        while (x <= y) {
            if (decision < 0) {
                x = x + 1;
                decision = decision + 2 * x + 1;
            } else {
                x = x + 1;
                y = y - 1;
                decision = decision + 2 * x + 1 - 2 * y;
            }
            Point p1 = new Point(modify.modifyx(x0 + x, gap), modify.modifyy(y0 + y, gap, offset));
            Point p2 = new Point(modify.modifyx(x0 + x, gap), modify.modifyy(y0 - y, gap, offset));
            Point p3 = new Point(modify.modifyx(x0 - x, gap), modify.modifyy(y0 + y, gap, offset));
            Point p4 = new Point(modify.modifyx(x0 - x, gap), modify.modifyy(y0 - y, gap, offset));
            Point p5 = new Point(modify.modifyx(x0 + y, gap), modify.modifyy(y0 + x, gap, offset));
            Point p6 = new Point(modify.modifyx(x0 + y, gap), modify.modifyy(y0 - x, gap, offset));
            Point p7 = new Point(modify.modifyx(x0 - y, gap), modify.modifyy(y0 + x, gap, offset));
            Point p8 = new Point(modify.modifyx(x0 - y, gap), modify.modifyy(y0 - x, gap, offset));
            g.fillRect(p1.x, p1.y, gap, gap);
            g.fillRect(p2.x, p2.y, gap, gap);
            g.fillRect(p3.x, p3.y, gap, gap);
            g.fillRect(p4.x, p4.y, gap, gap);
            g.fillRect(p5.x, p5.y, gap, gap);
            g.fillRect(p6.x, p6.y, gap, gap);
            g.fillRect(p7.x, p7.y, gap, gap);
            g.fillRect(p8.x, p8.y, gap, gap);
        }
    }
}