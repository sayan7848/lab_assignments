package myalgos;

import myops.modify;

import java.awt.*;

/**
 * Created by sayan on 31/8/16.
 */
public class mpline {

    public mpline(Graphics g, Point p1, Point p2, Color color, int gap, int offset)
    {
        int x1 = p1.x, x2 = p2.x;
        int y1 = p1.y, y2 = p2.y;
        int x = x1, y = y1;
        g.setColor(color);
        int dx = x2- x1;
        int dy = y2 - y1;
        int deltaE = 2 * dy, deltaNE = 2 * (dy - dx), d = 2 * dy - dx;
        g.fillRect(modify.modifyx(x1, gap), modify.modifyy(y1, gap, offset), gap, gap);
        while (x < x2)
        {
            if (d < 0)
            {
                d = d + deltaE;
                x = x + 1;
            }
            else
            {
                d = d + deltaNE;
                x = x + 1;
                y = y + 1;
            }
            g.fillRect(modify.modifyx(x, gap), modify.modifyy(y, gap, offset), gap, gap);
        }
    }
}
