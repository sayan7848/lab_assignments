package myalgos;

import myops.modify;

import java.awt.*;

/**
 * Created by sayan on 31/8/16.
 */
public class brasenham {

    public brasenham(Graphics g, Point p1, Point p2, Color color, int gap, int offset)
    {
        g.setColor(color);
        int x0 = p1.x;
        int y0 = p1.y;
        int x1 = p2.x;
        int y1 = p2.y;
        int dx = x1 - x0;
        int dy = y1 - y0;
        int p = 2 * dy - dx;
        for (int k = 0; k < dx; k++)
        {
            if(p < 0)
            {
                x0 = x0 + 1;
                g.fillRect(modify.modifyx(x0,gap),modify.modifyy(y0, gap, offset),gap,gap);
                p = p + 2 * dy;
            }
            else
            {
                x0 = x0 + 1;
                y0 = y0 + 1;
                g.fillRect(modify.modifyx(x0,gap),modify.modifyy(y0, gap, offset),gap,gap);
                p = p + 2 * dy - 2 * dx;
            }
        }
    }
}
