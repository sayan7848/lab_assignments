package myalgos;

import myops.modify;

import java.awt.*;

/**
 * Created by sayan on 31/8/16.
 */
public class mpellipse {

    public mpellipse(Graphics g, Point center, int rx, int ry, Color color, int gap, int offset)
    {
        g.setColor(color);
        int xc = center.x;
        int yc = center.y;
        int rxsq = rx * rx;
        int rysq = ry * ry;
        int x = 0, y = ry, px = 0, py = 2 * rxsq * y;
        drawEllipse(g, xc, yc, x, y, gap, offset);
        double p = rysq - (rxsq * ry) + (0.25 * rxsq);
        while(px < py)
        {
            x++;
            px += 2 * rysq;
            if(p < 0)
                p += rysq + px;
            else {
                y -= 1;
                py -= 2 * rxsq;
                p += rysq + px - py;
            }
            drawEllipse(g, xc, yc, x, y, gap, offset);
        }
        p = rysq * (Math.pow(x + 0.5, 2)) + rxsq * (Math.pow(y - 1, 2)) - rxsq * rysq;
        while (y > 0)
        {
            y -= 1;
            py = py - 2 * rxsq;
            if(p > 0)
                p += rxsq - py;
            else
            {
                x += 1;
                px += 2 * rysq;
                p += rxsq - py + px;
            }
            drawEllipse(g, xc, yc, x, y, gap, offset);
        }
    }

    private void drawEllipse(Graphics g, int xc, int yc, int x, int y, int gap, int offset)
    {
        g.fillRect(modify.modifyx(xc + x, gap), modify.modifyy(yc + y, gap, offset), gap, gap);
        g.fillRect(modify.modifyx(xc - x, gap), modify.modifyy(yc + y, gap, offset), gap, gap);
        g.fillRect(modify.modifyx(xc + x, gap), modify.modifyy(yc - y, gap, offset), gap, gap);
        g.fillRect(modify.modifyx(xc - x, gap), modify.modifyy(yc - y, gap, offset), gap, gap);
    }
}
