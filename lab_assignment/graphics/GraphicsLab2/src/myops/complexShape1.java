package myops;

import myalgos.mpcircle;

import java.awt.*;
import java.util.ArrayList;

/**
 * Created by sayan on 31/8/16.
 */
public class complexShape1 {

    private ArrayList<Point> pointArrayList1 = new ArrayList<>();
    private ArrayList<Point> pointArrayList2 = new ArrayList<>();
    private ArrayList<Point> pointArrayList3 = new ArrayList<>();
    private ArrayList<Point> pointArrayList4 = new ArrayList<>();

    public complexShape1(Graphics g, Point mainCenter, Point subCenter, int mainradius, int subradius, int tempradius, int num, Color color1, Color color2, Color color3, int gap, int offset)
    {
        new mpcircle(g, mainCenter, mainradius, color1, gap, offset);
        float theta = 90/num, basetheta=0f;
        int basex = subCenter.x;
        int basey = subCenter.y;
        while(basetheta <= 90)
        {
            int x1 = (int)(basex + tempradius * Math.sin(Math.toRadians(basetheta)));
            int y1 = (int)(basey + tempradius * Math.cos(Math.toRadians(basetheta)));
            int x2 = (int)(basex + tempradius * Math.sin(Math.toRadians(basetheta)));
            int y2 = (int)(basey - tempradius * Math.cos(Math.toRadians(basetheta)));
            int x3 = (int)(basex - tempradius * Math.sin(Math.toRadians(basetheta)));
            int y3 = (int)(basey - tempradius * Math.cos(Math.toRadians(basetheta)));
            int x4 = (int)(basex - tempradius * Math.sin(Math.toRadians(basetheta)));
            int y4 = (int)(basey + tempradius * Math.cos(Math.toRadians(basetheta)));
            pointArrayList1.add(new Point(x1, y1));
            pointArrayList2.add(new Point(x2, y2));
            pointArrayList3.add(new Point(x3, y3));
            pointArrayList4.add(new Point(x4, y4));
            basetheta += theta;
        }
        for (int i=0; i<pointArrayList1.size(); i++)
        {
            new mpcircle(g, pointArrayList1.get(i), subradius, color2, gap, offset);
            new mpcircle(g, pointArrayList2.get(i), subradius, color2, gap, offset);
            new mpcircle(g, pointArrayList3.get(i), subradius, color2, gap, offset);
            new mpcircle(g, pointArrayList4.get(i), subradius, color2, gap, offset);
        }
        g.setColor(color3);
        for (int i=0; i<pointArrayList1.size() - 1; i++)
        {
            int x1 = pointArrayList1.get(i).x;
            int y1 = pointArrayList1.get(i).y;
            int x2 = pointArrayList1.get(i + 1).x;
            int y2 = pointArrayList1.get(i + 1).y;
            //new brasenham(g, new Point(x1, y1), new Point(x2, y2), color3, gap, offset);
            g.drawLine(x1, y1, x2, y2);
        }
        for (int i=0; i<pointArrayList2.size() - 1; i++)
        {
            int x1 = pointArrayList2.get(i).x;
            int y1 = pointArrayList2.get(i).y;
            int x2 = pointArrayList2.get(i + 1).x;
            int y2 = pointArrayList2.get(i + 1).y;
           // new brasenham(g, new Point(x1, y1), new Point(x2, y2), color3, gap, offset);
            g.drawLine(x1, y1, x2, y2);
        }
        for (int i=0; i<pointArrayList3.size() - 1; i++)
        {
            int x1 = pointArrayList3.get(i).x;
            int y1 = pointArrayList3.get(i).y;
            int x2 = pointArrayList3.get(i + 1).x;
            int y2 = pointArrayList3.get(i + 1).y;
            //new brasenham(g, new Point(x1, y1), new Point(x2, y2), color3, gap, offset);
            g.drawLine(x1, y1, x2, y2);
        }
        for (int i=0; i<pointArrayList4.size() - 1; i++)
        {
            int x1 = pointArrayList4.get(i).x;
            int y1 = pointArrayList4.get(i).y;
            int x2 = pointArrayList4.get(i + 1).x;
            int y2 = pointArrayList4.get(i + 1).y;
            //new brasenham(g, new Point(x1, y1), new Point(x2, y2), color3, gap, offset);
            g.drawLine(x1, y1, x2, y2);
        }
    }
}
