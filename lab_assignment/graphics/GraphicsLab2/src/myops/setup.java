package myops;

import myapplet.applet;
import myhandlers.myActionHandler;

import java.awt.*;

/**
 * Created by sayan on 31/8/16.
 */
public class setup {

    public static void setupInit(applet applet,TextField[] textFields, Button[] buttons, int width, int height)
    {
        applet.setSize(width, height);
        for (int i=0;i<4;i++)
        {
            textFields[i] = new TextField(3);
            applet.add(textFields[i]);
        }
        buttons[0] = new Button("DDA");
        buttons[1] = new Button("Bras");
        buttons[2] = new Button("MPLine");
        buttons[3] = new Button("MPCircle");
        buttons[4] = new Button("MPEllipse");
        buttons[5] = new Button("Shape1");
        buttons[6] = new Button("ZoomIn");
        buttons[7] = new Button("ZoomOut");
        buttons[8] = new Button("Reset");
        buttons[9] = new Button("Clear");
        for (int i=0;i<buttons.length;i++)
        {
            buttons[i].addActionListener(new myActionHandler(applet, buttons, textFields));
            applet.add(buttons[i]);
        }
    }

    public static void drawGrid(Graphics g, int offset, Color color, int width, int height, int gap)
    {
        g.setColor(color);
        for(int i=0;i<width;i+=gap)
        {
            g.drawLine(i,offset,i,height);
        }
        for(int i=offset;i<height;i+=gap)
        {
            g.drawLine(0,i,width,i);
        }
        g.drawLine(width - 1,height - 1,width - 1,offset);
        g.drawLine(width - 1, height - 1,0, height - 1);
    }
}
