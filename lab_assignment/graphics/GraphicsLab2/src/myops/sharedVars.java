package myops;

import java.awt.*;

/**
 * Created by sayan on 31/8/16.
 */
public class sharedVars {

    public static Point[] ddapoint = new Point[2];
    public static Point[] brasenhampoint = new Point[2];
    public static Point[] mplinepoint = new Point[2];
    public static int[] mpcircleparams = new int[3];
    public static int[] mpellipseparams = new int[4];
    public static int[] shape1params = new int[4];
    public static boolean ddaflag = false;
    public static boolean bflag = false;
    public static boolean mplflag = false;
    public static boolean mpcflag = false;
    public static boolean mpeflag = false;
    public static boolean shapeflag = false;
    public static int shapeMainRad = 100;
    public static int shapeSubRad = 10;
    public static int shapeTempRad = 50;
    public static int gap = 2;
    public static int initialShapeMainRad = shapeMainRad;
    public static int initialShapeTempRad = shapeTempRad;
    public static int initialShapeSubRad = shapeSubRad;
    public static int initialCircRad;
    public static int initialEllipRx;
    public static int initialEllipRy;
}
