package myops;

/**
 * Created by sayan on 31/8/16.
 */
public class modify {

    public static int modifyx(int x, int gap)
    {
        return (x/gap) * gap;
    }

    public static int modifyy(int y, int gap, int offset)
    {
        return ((y - offset) / gap) * gap + offset;
    }
}
