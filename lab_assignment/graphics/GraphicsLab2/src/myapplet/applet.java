package myapplet;

import myalgos.*;
import myops.complexShape1;
import myops.setup;
import myops.sharedVars;

import java.applet.Applet;
import java.awt.*;

import static myops.sharedVars.*;

/**
 * Created by sayan on 31/8/16.
 */
public class applet extends Applet {

    private TextField[] textFields = new TextField[4];
    private Button[] buttons = new Button[10];
    private int width = 1366;
    private int height = 1000;

    public void init()
    {
        super.init();
        setup.setupInit(this, textFields, buttons, width, height);
    }

    public void paint(Graphics g)
    {
        int offset = 35;
        setup.drawGrid(g, offset, Color.BLACK, width, height, gap);
        if(sharedVars.ddaflag)
        {
            new dda(g, sharedVars.ddapoint[0], sharedVars.ddapoint[1], Color.BLUE, gap, offset);
        }
        if(sharedVars.bflag)
        {
            new brasenham(g, sharedVars.brasenhampoint[0], sharedVars.brasenhampoint[1], Color.RED, gap, offset);
        }
        if(sharedVars.mplflag)
        {
            new mpline(g, sharedVars.mplinepoint[0], sharedVars.mplinepoint[1], Color.MAGENTA, gap, offset);
        }
        if(sharedVars.mpcflag)
        {
            new mpcircle(g, new Point(sharedVars.mpcircleparams[0], sharedVars.mpcircleparams[1]), sharedVars.mpcircleparams[2], Color.GREEN, gap, offset);
        }
        if(sharedVars.mpeflag)
        {
            new mpellipse(g, new Point(sharedVars.mpellipseparams[0], sharedVars.mpellipseparams[1]), sharedVars.mpellipseparams[2], sharedVars.mpellipseparams[3], Color.ORANGE, gap, offset);
        }
        if(sharedVars.shapeflag)
        {
            new complexShape1(g, new Point(sharedVars.shape1params[0], sharedVars.shape1params[1]), new Point(sharedVars.shape1params[2], sharedVars.shape1params[3]), shapeMainRad, shapeSubRad, shapeTempRad, 2, Color.RED, Color.BLUE, Color.green, gap, offset);
        }
    }
}
