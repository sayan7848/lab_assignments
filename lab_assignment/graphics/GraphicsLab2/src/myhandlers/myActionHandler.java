package myhandlers;

import myapplet.applet;
import myops.sharedVars;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static myops.sharedVars.*;

/**
 * Created by sayan on 31/8/16.
 */
public class myActionHandler implements ActionListener {

    private applet appletInstance;
    private Button[] buttons;
    private TextField[] textFields;

    public myActionHandler(applet appletInstance, Button[] buttons, TextField[] textFields)
    {
        this.appletInstance = appletInstance;
        this.buttons = buttons;
        this.textFields = textFields;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int[] intarray = new int[4];
        if(e.getSource() == buttons[0])
        {
            for (int i=0;i<4;i++)
            {
                intarray[i] = Integer.parseInt(textFields[i].getText());
                System.out.println(intarray[i]);
            }
            sharedVars.ddapoint[0] = new Point(intarray[0], intarray[1]);
            sharedVars.ddapoint[1] = new Point(intarray[2], intarray[3]);
            sharedVars.ddaflag = true;
        }
        if(e.getSource() == buttons[1])
        {
            for (int i=0;i<4;i++)
            {
                intarray[i] = Integer.parseInt(textFields[i].getText());
            }
            sharedVars.brasenhampoint[0] = new Point(intarray[0], intarray[1]);
            sharedVars.brasenhampoint[1] = new Point(intarray[2], intarray[3]);
            sharedVars.bflag = true;
        }
        if(e.getSource() == buttons[2])
        {
            for (int i=0;i<4;i++)
            {
                intarray[i] = Integer.parseInt(textFields[i].getText());
            }
            sharedVars.mplinepoint[0] = new Point(intarray[0], intarray[1]);
            sharedVars.mplinepoint[1] = new Point(intarray[2], intarray[3]);
            sharedVars.mplflag = true;
        }
        if(e.getSource() == buttons[3])
        {
            for(int i=0; i<3; i++)
            {
                sharedVars.mpcircleparams[i] = Integer.parseInt(textFields[i].getText());
            }
            initialCircRad = mpcircleparams[2];
            sharedVars.mpcflag = true;
        }
        if(e.getSource() == buttons[4])
        {
            for(int i=0; i<4; i++)
            {
                sharedVars.mpellipseparams[i] = Integer.parseInt(textFields[i].getText());
            }
            initialEllipRx = mpellipseparams[2];
            initialEllipRy = mpellipseparams[3];
            sharedVars.mpeflag = true;
        }
        if(e.getSource() == buttons[5])
        {
            for (int i=0; i<4; i++)
            {
                sharedVars.shape1params[i] = Integer.parseInt(textFields[i].getText());
            }
            shapeflag = true;
        }
        if(e.getSource() == buttons[6])
        {
            if(gap <= 25)
            {
                gap ++;
                if(mpcflag)
                {
                    mpcircleparams[2] += gap;
                }
                if(mpeflag)
                {
                    mpellipseparams[2] += gap;
                    mpellipseparams[3] += gap;
                }
                if(shapeflag)
                {
                    shapeMainRad += gap;
                   // shapeSubRad += gap;
                    shapeTempRad += gap;
                }
            }
        }
        if(e.getSource() == buttons[7])
        {
            if(gap > 2)
            {
                gap --;
                if(mpcflag)
                {
                    mpcircleparams[2] -= gap;
                }
                if(mpeflag)
                {
                    mpellipseparams[2] -= gap;
                    mpellipseparams[3] -= gap;
                }
                if(shapeflag)
                {
                    shapeMainRad -= gap;
                   // shapeSubRad -= gap;
                    shapeTempRad -= gap;
                }
            }
        }
        if(e.getSource() == buttons[8])
        {
            gap = 2;
            if(mpcflag)
            {
                mpcircleparams[2] = initialCircRad;
            }
            if(mpeflag)
            {
                mpellipseparams[2] = initialEllipRx;
                mpellipseparams[3] = initialEllipRy;
            }
            if(shapeflag)
            {
                shapeMainRad = initialShapeMainRad;
                shapeSubRad = initialShapeSubRad;
                shapeTempRad = initialShapeTempRad;
            }
        }
        if(e.getSource() == buttons[9])
        {
            mplflag = false;
            ddaflag = false;
            bflag = false;
            mpcflag = false;
            mpeflag = false;
            shapeflag = false;
        }
        appletInstance.repaint();
    }
}
