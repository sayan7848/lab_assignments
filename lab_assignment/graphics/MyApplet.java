import java.applet.Applet;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;


/**
 * Created by sayan on 2/8/16.
 */
public class MyApplet extends Applet implements MouseListener, MouseMotionListener, ActionListener {

    private static final int width = 1366;
    private static final int height = 1000;
    private static final Button zoomIn = new Button("Zoom In");
    private static final Button zoomOut = new Button("Zoom Out");
    private static final Button resetZoom = new Button("Reset Zoom");
    private static final Button clearLine = new Button("Clear Line");
    private static final Button DDA = new Button("DDA");
    private static final Button Brasenham = new Button("Brasenham");
    private static final Button midPoint = new Button("Mid Point");
    private static final Button Circle = new Button("Circle");
    private static final Button Ellipse = new Button("Ellipse");
    private boolean isBrasenham = false;
    private boolean isMidPoint = false;
    private boolean isCircle = false;
    private boolean isEllipse = false;
    private boolean isDDA = false;
    private int gap = 2;
    private int offset = 35;
    private static final ArrayList<Point> coordinateList = new ArrayList<>();
    private static final ArrayList<Point> coordinateBrasenham = new ArrayList<>();
    private static final ArrayList<Point> coordinateMidPoint = new ArrayList<>();
    private static final ArrayList<Point> circleCenter = new ArrayList<>();
    private static final ArrayList<Point> ellipseData = new ArrayList<>();
    private static int circleRadius = 100;
    private int clickCount = 0;
    private static final TextField x1 = new TextField();
    private static final TextField y1 = new TextField();
    private static final TextField x2 = new TextField();
    private static final TextField y2 = new TextField();
    private int initialcirclerad, initialellipserx, initialellipsery;

    @Override
    public void init() {
        super.init();
        setSize(width, height);
        setBackground(Color.WHITE);
        Circle.addActionListener(this);
        zoomIn.addActionListener(this);
        zoomOut.addActionListener(this);
        resetZoom.addActionListener(this);
        clearLine.addActionListener(this);
        DDA.addActionListener(this);
        Brasenham.addActionListener(this);
        midPoint.addActionListener(this);
        Ellipse.addActionListener(this);
        addMouseListener(this);
        add(zoomIn);
        add(zoomOut);
        add(resetZoom);
        add(clearLine);
        add(DDA);
        add(Brasenham);
        add(midPoint);
        add(Circle);
        add(Ellipse);
        x1.setColumns(3);
        y1.setColumns(3);
        x2.setColumns(3);
        y2.setColumns(3);
        add(x1);
        add(y1);
        add(x2);
        add(y2);
    }

    @Override
    public void paint(Graphics g)
    {
        if (isDDA)
        {
            myDrawLine(g,coordinateList.get(0),coordinateList.get(1));
        }
        if (isBrasenham)
        {
            System.out.println("Brasenham");
            Brasenham(g,coordinateBrasenham.get(0),coordinateBrasenham.get(1));
        }
        if (isMidPoint)
        {
            System.out.println("Mid Point");
            midPoint(g, coordinateMidPoint.get(0), coordinateMidPoint.get(1));
        }
        if (isCircle)
        {
            System.out.println("Circle");
            midPointCircle(g,circleCenter.get(0));
        }
        if(isEllipse)
        {
            System.out.println("Ellipse");
            midPointEllipse(g, ellipseData.get(0).x, ellipseData.get(0).y, ellipseData.get(1).x, ellipseData.get(1).y);
        }
        g.setColor(Color.BLACK);
        for(int i=0;i<width;i+=gap)
        {
            g.drawLine(i,offset,i,height);
        }
        for(int i=offset;i<height;i+=gap)
        {
            g.drawLine(0,i,width,i);
        }
        g.drawLine(width - 1,height - 1,width - 1,offset);
        g.drawLine(width - 1, height - 1,0, height - 1);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == zoomIn) {
            if (gap < 30) {
                gap++;
                if (isCircle)
                    circleRadius += gap;
                if (isEllipse) {
                    int temprx = ellipseData.get(1).x;
                    int tempry = ellipseData.get(1).y;
                    temprx += gap;
                    tempry += gap;
                    ellipseData.set(1, new Point(temprx, tempry));
                }
            }
        } else if (e.getSource() == zoomOut) {
            if (gap > 2) {
                gap--;
                if (isCircle)
                    circleRadius -= gap;
                if (isEllipse) {
                    int temprx = ellipseData.get(1).x;
                    int tempry = ellipseData.get(1).y;
                    temprx -= gap;
                    tempry -= gap;
                    ellipseData.set(1, new Point(temprx, tempry));
                }
            }
        } else if (e.getSource() == resetZoom) {
            gap = 2;
            if (isCircle)
                circleRadius = initialcirclerad;
            if (isEllipse)
                ellipseData.set(1, new Point(initialellipserx, initialellipsery));
        }
            else if (e.getSource() == clearLine) {
                coordinateList.clear();
                coordinateBrasenham.clear();
                coordinateMidPoint.clear();
                circleCenter.clear();
                ellipseData.clear();
                clickCount = 0;
                isDDA = false;
                isBrasenham = false;
                isMidPoint = false;
                isCircle = false;
                isEllipse = false;
            } else if (e.getSource() == DDA) {
                coordinateList.add(new Point(Integer.parseInt(x1.getText()), Integer.parseInt(y1.getText()) + offset));
                coordinateList.add(new Point(Integer.parseInt(x2.getText()), Integer.parseInt(y2.getText()) + offset));
                isDDA = true;
            } else if (e.getSource() == Brasenham) {
                coordinateBrasenham.add(new Point(Integer.parseInt(x1.getText()), Integer.parseInt(y1.getText()) + offset));
                coordinateBrasenham.add(new Point(Integer.parseInt(x2.getText()), Integer.parseInt(y2.getText()) + offset));
                isBrasenham = true;
            } else if (e.getSource() == midPoint) {
                coordinateMidPoint.add(new Point(Integer.parseInt(x1.getText()), Integer.parseInt(y1.getText()) + offset));
                coordinateMidPoint.add(new Point(Integer.parseInt(x2.getText()), Integer.parseInt(y2.getText()) + offset));
                isMidPoint = true;
            } else if (e.getSource() == Circle) {
                circleCenter.add(new Point(Integer.parseInt(x1.getText()), Integer.parseInt(y1.getText()) + offset));
                circleRadius = Integer.parseInt(x2.getText());
                initialcirclerad = circleRadius;
                isCircle = true;
            } else if (e.getSource() == Ellipse) {
                ellipseData.add(new Point(Integer.parseInt(x1.getText()), Integer.parseInt(y1.getText()) + offset));
                ellipseData.add(new Point(Integer.parseInt(x2.getText()), Integer.parseInt(y2.getText())));
                initialellipserx = Integer.parseInt(x2.getText());
                initialellipsery = Integer.parseInt(y2.getText());
                isEllipse = true;
            }
            repaint();
        }


    @Override
    public void mouseClicked(MouseEvent e) {
        coordinateList.add(new Point(e.getX(),e.getY()));
        coordinateBrasenham.add(new Point(e.getX() + 100,e.getY() + 100));
        coordinateMidPoint.add(new Point(e.getX() + 200, e.getY() + 200));
        if(clickCount < 2)
            clickCount ++;
        if(clickCount == 2) {
            isDDA = true;
            isBrasenham = true;
            isMidPoint = true;
            repaint();
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseDragged(MouseEvent e) {

    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }

    private void myDrawLine(Graphics g, Point p1, Point p2)
    {
        int x1 = p1.x, y1 = p1.y, x2 = p2.x, y2 = p2.y;
        g.setColor(Color.BLUE);
        int dx,dy;
        float x = x1;
        float y = y1;
        int ymod = 0,f_x,f_y,x_mod,y_mod;
        dx=x2-x1;
        dy=y2-y1;
        int steps;
        if(dx>dy)
            steps=Math.abs(dx);
        else
            steps=Math.abs(dy);
        float x_inc,y_inc;
        x_inc=(float)dx/steps;
        y_inc=(float)dy/steps;
        for(int v=0;v<steps;v++){
            x+=x_inc;
            y+=y_inc;
            f_x=(int)Math.ceil(x);
            f_y=(int) Math.ceil(y);
            x_mod=(f_x/gap)*gap;
            y_mod=((f_y-offset)/gap)*gap+offset;
            try{
                if(Math.abs(y1-y2)>10){
                    if(y_mod!=ymod) {
                        g.fillRect(x_mod, y_mod, gap, gap);
                        ymod = y_mod;
                    }
                }
                else{
                    g.fillRect(x_mod, y_mod, gap, gap);
                }

            }
            catch(Exception e){
                e.printStackTrace();
            }
        }

    }

    private void Brasenham(Graphics g, Point p1, Point p2)
    {
        g.setColor(Color.RED);
        int x0 = p1.x;
        int y0 = p1.y;
        int x1 = p2.x;
        int y1 = p2.y;
        int dx = x1 - x0;
        int dy = y1 - y0;
        int p = 2 * dy - dx;
        for (int k = 0; k < dx; k++)
        {
            if(p < 0)
            {
                x0 = x0 + 1;
                g.fillRect(mapX(x0),mapY(y0),gap,gap);
                p = p + 2 * dy;
            }
            else
            {
                x0 = x0 + 1;
                y0 = y0 + 1;
                g.fillRect(mapX(x0),mapY(y0),gap,gap);
                p = p + 2 * dy - 2 * dx;
            }
        }
    }

    private void midPoint(Graphics g, Point p1, Point p2)
    {
        int x1 = p1.x, x2 = p2.x;
        int y1 = p1.y, y2 = p2.y;
        int x = x1, y = y1;
        g.setColor(Color.MAGENTA);
        int dx = x2- x1;
        int dy = y2 - y1;
        int deltaE = 2 * dy, deltaNE = 2 * (dy - dx), d = 2 * dy - dx;
        g.fillRect(mapX(x1), mapY(y1), gap, gap);
        while (x < x2)
        {
            if (d < 0)
            {
                d = d + deltaE;
                x = x + 1;
            }
            else
            {
                d = d + deltaNE;
                x = x + 1;
                y = y + 1;
            }
            g.fillRect(mapX(x), mapY(y), gap, gap);
        }
    }

    private void midPointCircle(Graphics g, Point p)
    {
        g.setColor(Color.CYAN);
        int radius = circleRadius;
        int x0 = p.x, y0 = p.y;
        int x = 0;
        int y = radius;
        float decision = 5 / 4 - radius;
        System.out.println(x);
        System.out.println(y);
        while (x <= y)
        {
            if(decision < 0)
            {
                x = x + 1;
                decision = decision + 2 * x + 1;
            }
            else
            {
                x = x + 1;
                y = y - 1;
                decision = decision + 2 * x + 1 - 2 * y;
            }
            Point p1 = new Point(mapX(x0 + x), mapY(y0 + y));
            Point p2 = new Point(mapX(x0 + x), mapY(y0 - y));
            Point p3 = new Point(mapX(x0 - x), mapY(y0 + y));
            Point p4 = new Point(mapX(x0 - x), mapY(y0 - y));
            Point p5 = new Point(mapX(x0 + y), mapY(y0 + x));
            Point p6 = new Point(mapX(x0 + y), mapY(y0 - x));
            Point p7 = new Point(mapX(x0 - y), mapY(y0 + x));
            Point p8 = new Point(mapX(x0 - y), mapY(y0 - x));
            g.fillRect(p1.x, p1.y, gap, gap);
            g.fillRect(p2.x, p2.y, gap, gap);
            g.fillRect(p3.x, p3.y, gap, gap);
            g.fillRect(p4.x, p4.y, gap, gap);
            g.fillRect(p5.x, p5.y, gap, gap);
            g.fillRect(p6.x, p6.y, gap, gap);
            g.fillRect(p7.x, p7.y, gap, gap);
            g.fillRect(p8.x, p8.y, gap, gap);
        }
    }

    private void midPointEllipse(Graphics g, int xc, int yc, int rx, int ry)
    {
        g.setColor(Color.GREEN);
        int rxsq = rx * rx;
        int rysq = ry * ry;
        int x = 0, y = ry, px = 0, py = 2 * rxsq * y;
        drawEllipse(g,xc,yc,x,y);
        double p = rysq - (rxsq * ry) + (0.25 * rxsq);
        while(px < py)
        {
            x++;
            px += 2 * rysq;
            if(p < 0)
                p += rysq + px;
            else {
                y -= 1;
                py -= 2 * rxsq;
                p += rysq + px - py;
            }
            drawEllipse(g, xc, yc, x, y);
        }
        p = rysq * (Math.pow(x + 0.5, 2)) + rxsq * (Math.pow(y - 1, 2)) - rxsq * rysq;
        while (y > 0)
        {
            y -= 1;
            py = py - 2 * rxsq;
            if(p > 0)
                p += rxsq - py;
            else
            {
                x += 1;
                px += 2 * rysq;
                p += rxsq - py + px;
            }
            drawEllipse(g, xc, yc, x, y);
        }
    }

    private void drawEllipse(Graphics g, int xc, int yc, int x, int y)
    {
        g.fillRect(mapX(xc + x), mapY(yc + y), gap, gap);
        g.fillRect(mapX(xc - x), mapY(yc + y), gap, gap);
        g.fillRect(mapX(xc + x), mapY(yc - y), gap, gap);
        g.fillRect(mapX(xc - x), mapY(yc - y), gap, gap);
    }

    private int mapX(int x)
    {
        return (x / gap) * gap;
    }

    private int mapY(int y)
    {
        return ((y - offset) / gap) * gap + offset;
    }
}
